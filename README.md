GIT MAGIC TALK
================

# Talk Resources
- [Slides](/slides.pdf).

# Usefull Links
- [Git Visualization](https://github.com/git-school/visualizing-git)
- [Git Book](https://git-scm.com/book/en/v2)
- [Made 2 Website](http://made2.co/)
